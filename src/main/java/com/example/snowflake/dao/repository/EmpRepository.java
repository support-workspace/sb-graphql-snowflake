package com.example.snowflake.dao.repository;

import com.example.snowflake.dao.entity.Emp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface EmpRepository extends JpaRepository<Emp, Integer> {

    @Query(value = "select eid as \"eid\", ename as \"ename\", sal as \"sal\", city as \"city\" from EMP", nativeQuery = true)
    List<Emp> findEmployees();

}
