package com.example.snowflake.dao.repository;

import com.example.snowflake.dao.entity.Student;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(value = "SELECT s.* FROM STUDENT s WHERE s.STUDENT_ID = ?", nativeQuery = true)
    Student findStudentById(@Param("student_id") Long studentId);

    @NotNull
    @Query(value = "SELECT s.* FROM STUDENT s", nativeQuery = true)
    List<Student> findAll();
}
