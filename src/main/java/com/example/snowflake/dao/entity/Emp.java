package com.example.snowflake.dao.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EMP")
@Getter
@Setter
@ToString
public class Emp {
    @Id
    @Column(name = "EID")
    private Integer eId;

    @Column(name = "ENAME")
    private String eName;

    @Column(name = "SAL")
    private Integer sal;

    @Column(name = "CITY")
    private String city;

}
