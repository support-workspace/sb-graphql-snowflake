package com.example.snowflake.dao.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT")
@Getter
@Setter
@ToString
public class Student {
    @Column(name = "STUDENT_NAME")
    private String studentName;

    @Id
    @Column(name = "STUDENT_ID")
    private Long studentId;

    @Column(name = "CLASS_ID")
    private String classId;

}
