package com.example.snowflake.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "datasources.myschema")
@Component(value = "databaseProperties")
@Data
public class DatabaseProperties {
    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
