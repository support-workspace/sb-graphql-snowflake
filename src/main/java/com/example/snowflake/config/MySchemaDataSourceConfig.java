package com.example.snowflake.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.example.snowflake.dao.repository"},
        entityManagerFactoryRef = "myschemaEntityManagerFactory",
        transactionManagerRef = "myschemaTransactionManager"
)
@EnableTransactionManagement
public class MySchemaDataSourceConfig {

    @Primary
    @Bean(name = "myschemaDataSource")
    public DataSource dataSource(@Qualifier("databaseProperties") DatabaseProperties databaseProperties) {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(databaseProperties.getUrl());
        dataSourceBuilder.username(databaseProperties.getUsername());
        dataSourceBuilder.password(databaseProperties.getPassword());
        dataSourceBuilder.driverClassName(databaseProperties.getDriverClassName());

        return dataSourceBuilder.build();
    }


    @Bean(name = "myschemaEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean customerEntityManager(@Qualifier("myschemaDataSource") DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.example.snowflake.dao.entity");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Primary
    @Bean(name = "myschemaTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("myschemaEntityManagerFactory") EntityManagerFactory entityManagerFactory
    ) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }


    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "com.example.snowflake.dialect.SnowflakeDialect");
        properties.setProperty("database-platform", "com.example.snowflake.dialect.SnowflakeDialect");
        properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        properties.setProperty("spring.jpa.database", "default");
        return properties;
    }

}
