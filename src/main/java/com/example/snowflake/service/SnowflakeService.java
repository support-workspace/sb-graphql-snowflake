package com.example.snowflake.service;

import com.example.snowflake.dao.entity.Student;
import com.example.snowflake.dao.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SnowflakeService {

    @Autowired
    StudentRepository studentRepository;


    public List<Student> findAllStudens() {
        return studentRepository.findAll();
    }

    public Student findStudentById(Long studentId) {
        return studentRepository.findStudentById(studentId);
    }
}
