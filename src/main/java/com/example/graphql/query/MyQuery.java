package com.example.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;

@Component
public class MyQuery implements GraphQLQueryResolver {

    public String myTest() {
        return "Hello, Welcome to GraphQL";
    }

    public int sum(int a, int b) {
        return a + b;
    }

}
