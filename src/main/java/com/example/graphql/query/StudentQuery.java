package com.example.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.snowflake.dao.entity.Student;
import com.example.snowflake.service.SnowflakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentQuery implements GraphQLQueryResolver {

    @Autowired
    SnowflakeService snowflakeService;

    public String studentTest() {
        return "Hello, Student Test !";
    }

    public String allStudents() {
        List<Student> students = snowflakeService.findAllStudens();
        StringBuilder studentsString = new StringBuilder();
        students.forEach(s -> studentsString.append(s.toString()).append("\n"));
        return studentsString.toString();
    }

    public String studentById(Long studentId) {
        Student students = snowflakeService.findStudentById(studentId);
        return students.toString();
    }


}
